package com.yml.banking.service;

import com.yml.banking.dto.request.AccountRequestDTO;
import com.yml.banking.dto.response.AccountResponseDTO;
import com.yml.banking.entity.Account;
import com.yml.banking.entity.Branch;
import com.yml.banking.entity.Customer;
import com.yml.banking.exception.BadRequestException;
import com.yml.banking.exception.DataNotFoundException;
import com.yml.banking.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class AccountService {

    private AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }


    private void setAccountDetails(AccountRequestDTO accountRequestDTO, Account account) {
        Branch branch = new Branch();
        Customer customer = new Customer();

        account.setAccountType(accountRequestDTO.getAccountType());
        account.setOpeningDate(accountRequestDTO.getOpeningDate());
        account.setAccountStatus(accountRequestDTO.getAccountStatus());
        account.setClosingDate(accountRequestDTO.getClosingDate());
        account.setAccountBalance(accountRequestDTO.getAccountBalance());

        branch.setBranchId(accountRequestDTO.getBranchId());
        customer.setCustomerId(accountRequestDTO.getCustomerId());

        account.setBranch(branch);
        account.setCustomer(customer);
    }


    private AccountResponseDTO getAccountResponseDTO(Account account) {
        return new AccountResponseDTO(account.getAccountId(),
                account.getAccountType(),
                account.getOpeningDate(),
                account.getAccountStatus(),
                account.getClosingDate(),
                account.getAccountBalance(),
                account.getBranch().getBranchId(),
                account.getCustomer().getCustomerId());
    }


    public List<AccountResponseDTO> getAccounts() {
        List<AccountResponseDTO> accountResponseDTOs = new ArrayList<>();
        List<Account> accounts = accountRepository.findAll();
        if(accounts.isEmpty()) {
            throw new DataNotFoundException("No accounts found");
        }
        for(Account account : accounts) {
            AccountResponseDTO accountResponseDTO = getAccountResponseDTO(account);
            accountResponseDTOs.add(accountResponseDTO);
        }
        return accountResponseDTOs;
    }


    public AccountResponseDTO getAccount(long accountId) {
        Account account =  accountRepository.findById(accountId).orElse(null);
        if(account == null) {
            throw new DataNotFoundException("Account not found");
        }
        return getAccountResponseDTO(account);
    }


    public AccountResponseDTO createAccount(AccountRequestDTO accountRequestDTO) {
        Account account = new Account();
        Account savedAccount;
        setAccountDetails(accountRequestDTO, account);
        try {
            savedAccount = accountRepository.save(account);
        } catch (DataIntegrityViolationException e) {
            throw new BadRequestException(Objects.requireNonNull(e.getRootCause()).getMessage());
        }
        return getAccountResponseDTO(savedAccount);
    }


    public AccountResponseDTO updateAccount(AccountRequestDTO accountRequestDTO, long accountId) {
        if(!accountRepository.findById(accountId).isPresent()) {
            throw new DataNotFoundException("Account not found");
        }
        Account account = new Account();
        Account savedAccount;
        account.setAccountId(accountId);
        setAccountDetails(accountRequestDTO, account);
        try {
            savedAccount = accountRepository.save(account);
        } catch (DataIntegrityViolationException e) {
            throw new BadRequestException(Objects.requireNonNull(e.getRootCause()).getMessage());
        }
        return getAccountResponseDTO(savedAccount);
    }


    public void deleteAccount(long accountId) {
        if(!accountRepository.findById(accountId).isPresent()) {
            throw new DataNotFoundException("Account not found");
        }
        accountRepository.deleteById(accountId);
    }

}
