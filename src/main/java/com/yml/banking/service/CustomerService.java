package com.yml.banking.service;

import com.yml.banking.dto.request.CustomerRequestDTO;
import com.yml.banking.dto.response.CustomerResponseDTO;
import com.yml.banking.entity.Customer;
import com.yml.banking.exception.BadRequestException;
import com.yml.banking.exception.DataNotFoundException;
import com.yml.banking.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class CustomerService {

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    private void setCustomerDetails(CustomerRequestDTO customerRequestDTO, Customer customer) {
        customer.setCustomerName(customerRequestDTO.getCustomerName());
        customer.setPassword(passwordEncoder.encode(customerRequestDTO.getPassword()));
        customer.setCustomerAge(customerRequestDTO.getCustomerAge());
        customer.setCustomerPhone(customerRequestDTO.getCustomerPhone());
        customer.setCustomerEmail(customerRequestDTO.getCustomerEmail());
        customer.setCustomerAddress(customerRequestDTO.getCustomerAddress());
        customer.setCustomerZipCode(customerRequestDTO.getCustomerZipCode());
        customer.setPanNumber(customerRequestDTO.getPanNumber());
    }

    private CustomerResponseDTO getCustomerResponseDTO(Customer customer) {
        return new CustomerResponseDTO(customer.getCustomerId(),
                customer.getCustomerName(),
                customer.getCustomerAge(),
                customer.getCustomerPhone(),
                customer.getCustomerEmail(),
                customer.getCustomerAddress(),
                customer.getCustomerZipCode(),
                customer.getPanNumber());
    }

    public List<CustomerResponseDTO> getCustomers() {
        List<CustomerResponseDTO> customerResponseDTOs = new ArrayList<>();
        List<Customer> customers =  customerRepository.findAll();
        if(customers.isEmpty()) {
            throw new DataNotFoundException("No customers found");
        }
        for(Customer customer : customers) {
            CustomerResponseDTO customerResponseDTO = getCustomerResponseDTO(customer);
            customerResponseDTOs.add(customerResponseDTO);
        }
        return customerResponseDTOs;
    }

    public CustomerResponseDTO getCustomer(int customerId) {
        Customer customer = customerRepository.findById(customerId).orElse(null);
        if(customer == null) {
            throw new DataNotFoundException("Customer not found");
        }
        return getCustomerResponseDTO(customer);
    }

    public CustomerResponseDTO createCustomer(CustomerRequestDTO customerRequestDTO) {
        Customer customer = new Customer();
        Customer savedCustomer;
        setCustomerDetails(customerRequestDTO, customer);

        try {
            savedCustomer = customerRepository.save(customer);
        } catch (DataIntegrityViolationException e) {
            throw new BadRequestException(Objects.requireNonNull(e.getRootCause()).getMessage());
        }

        return getCustomerResponseDTO(savedCustomer);
    }


    public CustomerResponseDTO updateCustomer(CustomerRequestDTO customerRequestDTO, int customerId) {
        if(!customerRepository.findById(customerId).isPresent()) {
            throw new DataNotFoundException("Customer not found");
        }

        Customer customer = new Customer();
        Customer savedCustomer;
        customer.setCustomerId(customerId);
        setCustomerDetails(customerRequestDTO, customer);

        try {
            savedCustomer = customerRepository.save(customer);
        } catch (DataIntegrityViolationException e) {
            throw new BadRequestException(Objects.requireNonNull(e.getRootCause()).getMessage());
        }

        return getCustomerResponseDTO(savedCustomer);

    }

    public void deleteCustomer(int customerId, HttpServletResponse httpServletResponse) {
        if(!customerRepository.findById(customerId).isPresent()) {
            throw new DataNotFoundException("Customer not found");
        }

        customerRepository.deleteById(customerId);
        httpServletResponse.setStatus(HttpServletResponse.SC_CREATED);
    }

}
