package com.yml.banking.service;

import com.yml.banking.dto.request.EmployeeRequestDTO;
import com.yml.banking.dto.response.EmployeeResponseDTO;
import com.yml.banking.entity.Branch;
import com.yml.banking.entity.Employee;
import com.yml.banking.exception.BadRequestException;
import com.yml.banking.exception.DataNotFoundException;
import com.yml.banking.repository.BranchRepository;
import com.yml.banking.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class EmployeeService {

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private BranchRepository branchRepository;
    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeService(BranchRepository branchRepository, EmployeeRepository employeeRepository) {
        this.branchRepository = branchRepository;
        this.employeeRepository = employeeRepository;
    }


    private void setEmployeeDetails(EmployeeRequestDTO employeeRequestDTO, Employee employee) {
        Branch branch = new Branch();
        employee.setEmployeeName(employeeRequestDTO.getEmployeeName());
        employee.setPassword(passwordEncoder.encode(employeeRequestDTO.getPassword()));
        employee.setEmployeeAge(employeeRequestDTO.getEmployeeAge());
        employee.setEmployeePhone(employeeRequestDTO.getEmployeePhone());
        employee.setEmployeeEmail(employeeRequestDTO.getEmployeeEmail());
        employee.setEmployeeAddress(employeeRequestDTO.getEmployeeAddress());
        employee.setEmployeeZipCode(employeeRequestDTO.getEmployeeZipCode());
        employee.setDesignation(employeeRequestDTO.getDesignation());
        employee.setEmployeeSalary(employeeRequestDTO.getEmployeeSalary());
        branch.setBranchId(employeeRequestDTO.getBranchId());
        employee.setBranch(branch);
    }


    private EmployeeResponseDTO getEmployeeResponseDTO(Employee employee) {
        return new EmployeeResponseDTO(employee.getEmployeeId(),
                employee.getEmployeeName(),
                employee.getEmployeeAge(), employee.getEmployeePhone(),
                employee.getEmployeeEmail(),
                employee.getEmployeeAddress(),
                employee.getEmployeeZipCode(),
                employee.getDesignation(),
                employee.getEmployeeSalary(),
                employee.getBranch().getBranchId());
    }


    public List<EmployeeResponseDTO> getEmployees() {
        List<EmployeeResponseDTO> employeeResponseDTOs = new ArrayList<>();
        List<Employee> employees = employeeRepository.findAll();
        if(employees.isEmpty()) {
            throw new DataNotFoundException("No employees found");
        }
        for(Employee employee: employees) {
            EmployeeResponseDTO employeeResponseDTO = getEmployeeResponseDTO(employee);
            employeeResponseDTOs.add(employeeResponseDTO);
        }
        return employeeResponseDTOs;
    }


    public EmployeeResponseDTO getEmployee(int employeeId) {
        Employee employee = employeeRepository.findById(employeeId).orElse(null);
        if(employee == null) {
            throw new DataNotFoundException("Employee not found");
        }
        return getEmployeeResponseDTO(employee);
    }


    public EmployeeResponseDTO createEmployee(EmployeeRequestDTO employeeRequestDTO) {
        Employee employee = new Employee();
        Employee savedEmployee;
        setEmployeeDetails(employeeRequestDTO, employee);
        try {
            savedEmployee = employeeRepository.save(employee);
        } catch (DataIntegrityViolationException e) {
            throw new BadRequestException(Objects.requireNonNull(e.getRootCause()).getMessage());
        }
        return getEmployeeResponseDTO(savedEmployee);
    }


    public EmployeeResponseDTO updateEmployee(EmployeeRequestDTO employeeRequestDTO, int employeeId) {
        if(!employeeRepository.findById(employeeId).isPresent()) {
            throw new DataNotFoundException("Employee not found");
        }
        Employee employee = new Employee();
        Employee savedEmployee;
        employee.setEmployeeId(employeeId);
        setEmployeeDetails(employeeRequestDTO, employee);
        try {
            savedEmployee = employeeRepository.save(employee);
        } catch (DataIntegrityViolationException e) {
            throw new BadRequestException(Objects.requireNonNull(e.getRootCause()).getMessage());
        }
        return getEmployeeResponseDTO(savedEmployee);
    }


    public void deleteEmployee(int employeeId, HttpServletResponse httpServletResponse) {
        if(!employeeRepository.findById(employeeId).isPresent()) {
            throw new DataNotFoundException("Employee not found");
        }

        employeeRepository.deleteById(employeeId);
        httpServletResponse.setStatus(HttpServletResponse.SC_CREATED);
    }


    public List<EmployeeResponseDTO> getEmployeesByBranch(int branchId) {
        List<EmployeeResponseDTO> employeeResponseDTOs = new ArrayList<>();

        Branch branch = branchRepository.findBranchByBranchId(branchId);
        if(branch == null) {
            throw new DataNotFoundException("Branch not found");

        }
        List<Employee> employees = employeeRepository.findEmployeesByBranch(branch);

        if(employees.isEmpty()) {
            throw new DataNotFoundException("No employees found");
        }
        for(Employee employee: employees) {
            EmployeeResponseDTO employeeResponseDTO = getEmployeeResponseDTO(employee);
            employeeResponseDTOs.add(employeeResponseDTO);
        }
        return employeeResponseDTOs;
    }
}
