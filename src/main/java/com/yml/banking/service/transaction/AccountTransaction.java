package com.yml.banking.service.transaction;

import java.util.Arrays;
import java.util.List;

public class AccountTransaction implements TransactionFunctionality {

    float status = 0;

    public List<Float> withdraw(float withdrawalAmount, float balanceAmount ) {

        if(balanceAmount < withdrawalAmount) {
            System.out.println("Insufficient Funds");
            status = 2;
            return Arrays.asList(status);
        }

        else {
            balanceAmount -= withdrawalAmount;
            status = 1;
            return Arrays.asList(status, balanceAmount);
        }

    }

    public List<Float> deposit(float depositAmount, float balanceAmount ) {

        balanceAmount += depositAmount;
        status = 1;
        return Arrays.asList(status, balanceAmount);

    }


    public List<Float> transfer(float transferAmount, float fromBalanceAmount, float toBalanceAmount ) {

        float status = 0;

        //ensure sufficient balance for withdrawal
        if(fromBalanceAmount < transferAmount) {
            System.out.println("Insufficient Funds");
            status = 2;
            return Arrays.asList(status);
        }
        else {
            fromBalanceAmount -= transferAmount;        //subtract transfer amount from sender's balance
            toBalanceAmount += transferAmount;          //add transfer amount to receiver's balance
            status = 1;
            return Arrays.asList(status, fromBalanceAmount, toBalanceAmount);
        }

    }

}
