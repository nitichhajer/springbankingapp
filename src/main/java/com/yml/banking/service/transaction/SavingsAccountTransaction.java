package com.yml.banking.service.transaction;

import com.yml.banking.exception.BadRequestException;

import java.util.Arrays;
import java.util.List;

public class SavingsAccountTransaction extends AccountTransaction {

    private float withdrawalLimit = 100000;


    @Override
    public List<Float> withdraw(float withdrawalAmount, float balanceAmount) {
        if(withdrawalAmount > withdrawalLimit) {
            status = 2;
            return Arrays.asList(status);
        }
        return super.withdraw(withdrawalAmount, balanceAmount);
    }

    @Override
    public List<Float> transfer(float transferAmount, float fromBalanceAmount, float toBalanceAmount) {
//        System.out.println("Not Allowed");
//        status = 2;
//        return Arrays.asList(status);
        throw new BadRequestException("Transfer not possible for Savings Account");
    }
}
