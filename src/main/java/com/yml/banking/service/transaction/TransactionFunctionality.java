package com.yml.banking.service.transaction;

import java.util.List;

public interface TransactionFunctionality {

    public List<Float> withdraw(float withdrawalAmount, float balanceAmount);
    public List<Float> deposit(float withdrawalAmount, float balanceAmount);
    public List<Float> transfer(float transferAmount, float fromBalanceAmount, float toBalanceAmount );

}
