package com.yml.banking.service.transaction;

public class AccountTransactionFactory {

    public AccountTransaction getAccountTransaction(String accountType) {

        if(accountType.equals("Savings")) {
            return new SavingsAccountTransaction();
        }

        else if(accountType.equals("Current")) {
            return new CurrentAccountTransaction();
        }

        else {
            return null;
        }

    }

}
