package com.yml.banking.service.transaction;

import com.yml.banking.dto.request.TransactionRequestDTO;
import com.yml.banking.dto.response.TransactionResponseDTO;
import com.yml.banking.entity.Account;
import com.yml.banking.entity.Transaction;
import com.yml.banking.exception.BadRequestException;
import com.yml.banking.exception.DataNotFoundException;
import com.yml.banking.exception.InternalServerException;
import com.yml.banking.repository.AccountRepository;
import com.yml.banking.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TransactionService {

    private static Map<Float, String> statusMap = new HashMap<>();

    static {
        statusMap.put((float) 1, "Successful");
        statusMap.put((float) 2, "Failed");
        statusMap.put((float) 3, "Processing");
    }


    private TransactionRepository transactionRepository;
    private AccountRepository accountRepository;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository, AccountRepository accountRepository) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }


    private TransactionResponseDTO getTransactionResponseDTO(Transaction transaction) {

        TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO(transaction.getTransactionId(),
                transaction.getTransactionType(),
                transaction.getTransactionDateTime(),
                transaction.getFromAccount().getAccountId(),
                transaction.getTransactionAmount(),
                transaction.getTransactionStatus());
        if(transaction.getToAccount() != null) {
            transactionResponseDTO.setToAccountId(transaction.getToAccount().getAccountId());
        }
        return transactionResponseDTO;

    }


    private Transaction withdrawAmount(TransactionFunctionality accountTransaction, float transactionAmount, float fromBalanceAmount, long fromAccountId) {
        Transaction transaction = new Transaction();

        List<Float> updates = accountTransaction.withdraw(transactionAmount, fromBalanceAmount);
        if(updates.get(0) == 1) {
            accountRepository.updateAccountBalanceByAccountId(fromAccountId, updates.get(1));
        }
        transaction.setTransactionStatus(statusMap.get(updates.get(0)));

        return transaction;
    }


    private Transaction depositAmount(TransactionFunctionality accountTransaction, float transactionAmount, float fromBalanceAmount, long fromAccountId) {
        Transaction transaction = new Transaction();

        List<Float> updates = accountTransaction.deposit(transactionAmount, fromBalanceAmount);
        if(updates.get(0) == 1) {
            accountRepository.updateAccountBalanceByAccountId(fromAccountId, updates.get(1));
        }
        transaction.setTransactionStatus(statusMap.get(updates.get(0)));

        return transaction;
    }


    private Transaction transferAmount(TransactionRequestDTO transactionRequestDTO, TransactionFunctionality accountTransaction, float transactionAmount, float fromBalanceAmount, long fromAccountId) {
        Transaction transaction = new Transaction();

        long toAccountId = transactionRequestDTO.getToAccountId();
        Account toAccount = accountRepository.findById(toAccountId).orElse(null);
        if(toAccount == null) {
            throw new  DataNotFoundException("Receiver Account ID is Invalid");
        }
        String toAccountType = toAccount.getAccountType();
        float toBalanceAmount = toAccount.getAccountBalance();

        AccountTransactionFactory accountTransactionFactory = new AccountTransactionFactory();
        TransactionFunctionality accountTransactionCheck = accountTransactionFactory.getAccountTransaction(toAccountType);
        if(accountTransaction.getClass() != accountTransactionCheck.getClass()) {
            throw new BadRequestException("Transfer not possible for differing account types");
        }

        List<Float> updates = accountTransaction.transfer(transactionAmount, fromBalanceAmount, toBalanceAmount);
        if(updates.get(0) == 1) {
            accountRepository.updateAccountBalanceByAccountId(fromAccountId, updates.get(1));
            accountRepository.updateAccountBalanceByAccountId(toAccountId, updates.get(2));
        }
        transaction.setToAccount(toAccount);
        transaction.setTransactionStatus(statusMap.get(updates.get(0)));

        return transaction;

    }


    public List<TransactionResponseDTO> getTransactions() {
        List<TransactionResponseDTO> transactionResponseDTOs = new ArrayList<>();
        List<Transaction> transactions = transactionRepository.findAll();
        if(transactions.isEmpty()) {
            throw new DataNotFoundException("No transactions found");
        }
        for(Transaction transaction : transactions) {
            TransactionResponseDTO transactionResponseDTO = getTransactionResponseDTO(transaction);
            transactionResponseDTOs.add(transactionResponseDTO);
        }
        return transactionResponseDTOs;
    }


    public TransactionResponseDTO getTransaction(long transactionId) {
        Transaction transaction = transactionRepository.findById(transactionId).orElse(null);
        if(transaction == null) {
            throw new DataNotFoundException("Transaction not found");
        }
        return getTransactionResponseDTO(transaction);
    }


    public TransactionResponseDTO performTransaction(TransactionRequestDTO transactionRequestDTO) {
        Transaction transaction;
        Transaction savedTransaction;

        String transactionType = transactionRequestDTO.getTransactionType().toLowerCase();
        long fromAccountId = transactionRequestDTO.getFromAccountId();
        float transactionAmount = transactionRequestDTO.getTransactionAmount();

        Account fromAccount = accountRepository.findById(fromAccountId).orElse(null);
        if(fromAccount == null) {
            throw new  DataNotFoundException("Your Account ID is Invalid");
        }
        String fromAccountType = fromAccount.getAccountType();
        float fromBalanceAmount = fromAccount.getAccountBalance();

        AccountTransactionFactory accountTransactionFactory = new AccountTransactionFactory();
        TransactionFunctionality accountTransaction = accountTransactionFactory.getAccountTransaction(fromAccountType);
        if(accountTransaction == null) {
            throw new InternalServerException("Cannot perform operation");
        }

        switch (transactionType) {
            case "withdraw":
                transaction = withdrawAmount(accountTransaction, transactionAmount, fromBalanceAmount, fromAccountId);
                break;
            case "deposit":
                transaction = depositAmount(accountTransaction, transactionAmount, fromBalanceAmount, fromAccountId);
                break;
            case "transfer":
                transaction = transferAmount(transactionRequestDTO, accountTransaction, transactionAmount, fromBalanceAmount, fromAccountId);
                break;
            default:
                throw new BadRequestException("Invalid Transaction Type");
        }

        transaction.setTransactionType(transactionType);
        transaction.setTransactionDateTime(new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()));
        transaction.setFromAccount(fromAccount);
        transaction.setTransactionAmount(transactionAmount);

        try {
            savedTransaction = transactionRepository.save(transaction);
        } catch (DataIntegrityViolationException e) {
            throw new BadRequestException(Objects.requireNonNull(e.getRootCause()).getMessage());
        }

        return getTransactionResponseDTO(savedTransaction);
    }

}

