package com.yml.banking.service;

import com.yml.banking.dto.request.BranchRequestDTO;
import com.yml.banking.dto.response.BranchResponseDTO;
import com.yml.banking.entity.Branch;
import com.yml.banking.exception.BadRequestException;
import com.yml.banking.exception.DataNotFoundException;
import com.yml.banking.repository.BranchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class BranchService {

    private BranchRepository branchRepository;

    @Autowired
    public BranchService(BranchRepository branchRepository) {
        this.branchRepository = branchRepository;
    }


    private void setBranchDetails(BranchRequestDTO branchRequestDTO, Branch branch) {
        branch.setBranchName(branchRequestDTO.getBranchName());
        branch.setBranchAddress(branchRequestDTO.getBranchAddress());
        branch.setBranchZipCode(branchRequestDTO.getBranchZipCode());
        branch.setBranchIfsc(branchRequestDTO.getBranchIfsc());
    }


    private BranchResponseDTO getBranchResponseDTO(Branch branch) {
        return new BranchResponseDTO(branch.getBranchId(),
                branch.getBranchName(),
                branch.getBranchAddress(),
                branch.getBranchZipCode(),
                branch.getBranchIfsc());
    }


    public List<BranchResponseDTO> getBranches() {
        List<BranchResponseDTO> branchResponseDTOs = new ArrayList<>();
        List<Branch> branches = branchRepository.findAll();
        if(branches.isEmpty()) {
            throw new DataNotFoundException("No branches found");
        }
        for(Branch branch : branches) {
            BranchResponseDTO branchResponseDTO = getBranchResponseDTO(branch);
            branchResponseDTOs.add(branchResponseDTO);
        }
        return branchResponseDTOs;
    }


    public BranchResponseDTO getBranch(int branchId) {
        Branch branch = branchRepository.findById(branchId).orElse(null);
        if(branch == null) {
            throw new DataNotFoundException("Branch not found");
        }
        return getBranchResponseDTO(branch);
    }


    public BranchResponseDTO createBranch(BranchRequestDTO branchRequestDTO) {
        Branch branch = new Branch();
        Branch savedBranch;
        setBranchDetails(branchRequestDTO, branch);
        try {
            savedBranch =  branchRepository.save(branch);
        } catch (DataIntegrityViolationException e) {
            e.printStackTrace();
            throw new BadRequestException(Objects.requireNonNull(e.getRootCause()).getMessage());
        }
        return getBranchResponseDTO(savedBranch);
    }


    public BranchResponseDTO updateBranch(BranchRequestDTO branchRequestDTO, int branchId) {
        if(!branchRepository.findById(branchId).isPresent()) {
            throw new DataNotFoundException("Branch not found");
        }
        Branch branch = new Branch();
        Branch savedBranch;
        branch.setBranchId(branchId);
        setBranchDetails(branchRequestDTO, branch);
        try {
            savedBranch =  branchRepository.save(branch);
        } catch (DataIntegrityViolationException e) {
            throw new BadRequestException(Objects.requireNonNull(e.getRootCause()).getMessage());
        }
        return getBranchResponseDTO(savedBranch);
    }


    public void deleteBranch(int branchId, HttpServletResponse httpServletResponse) {
        if(!branchRepository.findById(branchId).isPresent()) {
            throw new DataNotFoundException("Branch not found");
        }

        branchRepository.deleteById(branchId);
        httpServletResponse.setStatus(HttpServletResponse.SC_CREATED);
    }

}
