package com.yml.banking.security;

import com.yml.banking.repository.CustomerRepository;
import com.yml.banking.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.lang.reflect.Method;
import java.util.HashMap;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private UserDetailsService userDetailsService;
    private CustomerRepository customerRepository;
    private EmployeeRepository employeeRepository;

    @Autowired
    public SecurityConfiguration(UserDetailsService userDetailsService, CustomerRepository customerRepository, EmployeeRepository employeeRepository) {
        this.userDetailsService = userDetailsService;
        this.customerRepository = customerRepository;
        this.employeeRepository = employeeRepository;
    }


    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.authenticationProvider(authenticationProvider());
    }


    private DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        return daoAuthenticationProvider;
    }


    @Bean
    public PasswordEncoder passwordEncoder(){
        return new  BCryptPasswordEncoder();
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .csrf().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
//                .httpBasic()
//                .and()
                .addFilter(new JwtAuthenticationFilter(authenticationManager()))
                .addFilter(new JwtAuthorizationFilter(authenticationManager(), customerRepository, employeeRepository))
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/branches/**").permitAll()
                .antMatchers("/branches/**").hasRole("EMPLOYEE")
                .antMatchers("/employees/**").hasRole("EMPLOYEE")
                .antMatchers(HttpMethod.POST, "/customer/**").permitAll()
                .antMatchers("/customers/**").hasAnyRole("CUSTOMER", "EMPLOYEE")
                .antMatchers("/accounts/**").hasAnyRole("CUSTOMER", "EMPLOYEE")
                .antMatchers("/transactions/**").hasAnyRole("CUSTOMER", "EMPLOYEE");
    }

}

