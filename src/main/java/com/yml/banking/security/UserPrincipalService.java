package com.yml.banking.security;

import com.yml.banking.entity.Customer;
import com.yml.banking.entity.Employee;
import com.yml.banking.repository.CustomerRepository;
import com.yml.banking.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserPrincipalService implements UserDetailsService {

    private CustomerRepository customerRepository;
    private EmployeeRepository employeeRepository;

    @Autowired
    public UserPrincipalService(CustomerRepository customerRepository, EmployeeRepository employeeRepository) {
        this.customerRepository = customerRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Customer customer = (Customer) customerRepository.findByCustomerEmail(username).orElse(null);
        if (customer != null) {
            return new CustomerPrincipal(customer);
        }

        Employee employee = (Employee) employeeRepository.findByEmployeeEmail(username).orElse(null);
        if (employee != null) {
            return new EmployeePrincipal(employee);
        }
        throw new UsernameNotFoundException("Invalid ID");
    }

}
