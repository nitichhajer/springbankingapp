package com.yml.banking.security;


import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.yml.banking.entity.Customer;
import com.yml.banking.entity.Employee;
import com.yml.banking.exception.InternalServerException;
import com.yml.banking.repository.CustomerRepository;
import com.yml.banking.repository.EmployeeRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private CustomerRepository customerRepository;
    private EmployeeRepository employeeRepository;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager, CustomerRepository customerRepository, EmployeeRepository employeeRepository) {
        super(authenticationManager);
        this.customerRepository = customerRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        String header = request.getHeader(JwtProperties.HEADER_STRING);

        if(header == null || !header.startsWith(JwtProperties.TOKEN_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }

        Authentication authentication = null;
        try {
            authentication = getUsernamePasswordAuthentication(request);
        } catch (TokenExpiredException e) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.getWriter().print("Token Expired");
            return;
        } catch (Exception e){
            e.printStackTrace();
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);

    }

    private Authentication getUsernamePasswordAuthentication(HttpServletRequest request) throws TokenExpiredException {

        UserDetails userPrincipal = null;

        String token = request.getHeader(JwtProperties.HEADER_STRING);

        if (token == null) {
            return null;
        }

        String username = JWT.require(Algorithm.HMAC512(JwtProperties.SECRET.getBytes()))
                .build()
                .verify(token.replace(JwtProperties.TOKEN_PREFIX, ""))
                .getSubject();

        if (username == null) {
            return null;
        }

        Customer customer = (Customer) customerRepository.findByCustomerEmail(username).orElse(null);
        if(customer != null) {
            userPrincipal = new CustomerPrincipal(customer);
        }

        else {
            Employee employee = (Employee) employeeRepository.findByEmployeeEmail(username).orElse(null);
            if (employee != null) {
                userPrincipal = new EmployeePrincipal(employee);
            }
            else {
                return null;
            }
        }

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, null, userPrincipal.getAuthorities());
        return authenticationToken;


    }
}
