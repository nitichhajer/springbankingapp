package com.yml.banking.dto.response;

public class AccountResponseDTO {

    private long accountId;
    private String accountType;
    private String openingDate;
    private String accountStatus;
    private String closingDate;
    private float accountBalance;
    private int branchId;
    private int customerId;

    public AccountResponseDTO(long accountId, String accountType, String openingDate, String accountStatus, String closingDate, float accountBalance, int branchId, int customerId) {
        this.accountId = accountId;
        this.accountType = accountType;
        this.openingDate = openingDate;
        this.accountStatus = accountStatus;
        this.closingDate = closingDate;
        this.accountBalance = accountBalance;
        this.branchId = branchId;
        this.customerId = customerId;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(String openingDate) {
        this.openingDate = openingDate;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(String closingDate) {
        this.closingDate = closingDate;
    }

    public float getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(float accountBalance) {
        this.accountBalance = accountBalance;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

}
