package com.yml.banking.dto.response;

public class EmployeeResponseDTO {

    private int employeeId;
    private String employeeName;
    private int employeeAge;
    private String employeePhone;
    private String employeeEmail;
    private String employeeAddress;
    private int employeeZipCode;
    private String designation;
    private long employeeSalary;
    private int branchId;

    public EmployeeResponseDTO(int employeeId, String employeeName, int employeeAge, String employeePhone, String employeeEmail, String employeeAddress, int employeeZipCode, String designation, long employeeSalary, int branchId) {
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.employeeAge = employeeAge;
        this.employeePhone = employeePhone;
        this.employeeEmail = employeeEmail;
        this.employeeAddress = employeeAddress;
        this.employeeZipCode = employeeZipCode;
        this.designation = designation;
        this.employeeSalary = employeeSalary;
        this.branchId = branchId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public int getEmployeeAge() {
        return employeeAge;
    }

    public void setEmployeeAge(int employeeAge) {
        this.employeeAge = employeeAge;
    }

    public String getEmployeePhone() {
        return employeePhone;
    }

    public void setEmployeePhone(String employeePhone) {
        this.employeePhone = employeePhone;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getEmployeeAddress() {
        return employeeAddress;
    }

    public void setEmployeeAddress(String employeeAddress) {
        this.employeeAddress = employeeAddress;
    }

    public int getEmployeeZipCode() {
        return employeeZipCode;
    }

    public void setEmployeeZipCode(int employeeZipCode) {
        this.employeeZipCode = employeeZipCode;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public long getEmployeeSalary() {
        return employeeSalary;
    }

    public void setEmployeeSalary(long employeeSalary) {
        this.employeeSalary = employeeSalary;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }
}
