package com.yml.banking.dto.response;

public class BranchResponseDTO {

    private int branchId;
    private String branchName;
    private String branchAddress;
    private int branchZipCode;
    private String branchIfsc;

    public BranchResponseDTO(int branchId, String branchName, String branchAddress, int branchZipCode, String branchIfsc) {
        this.branchId = branchId;
        this.branchName = branchName;
        this.branchAddress = branchAddress;
        this.branchZipCode = branchZipCode;
        this.branchIfsc = branchIfsc;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    public int getBranchZipCode() {
        return branchZipCode;
    }

    public void setBranchZipCode(int branchZipCode) {
        this.branchZipCode = branchZipCode;
    }

    public String getBranchIfsc() {
        return branchIfsc;
    }

    public void setBranchIfsc(String branchIfsc) {
        this.branchIfsc = branchIfsc;
    }

}
