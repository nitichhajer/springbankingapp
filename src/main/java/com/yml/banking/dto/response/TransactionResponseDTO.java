package com.yml.banking.dto.response;

public class TransactionResponseDTO {

    private long transactionId;
    private String transactionType;
    private String transactionDateTime;
    private long fromAccountId;
    private long toAccountId;
    private float transactionAmount;
    private String transactionStatus;

    public TransactionResponseDTO(long transactionId, String transactionType, String transactionDateTime, long fromAccountId, float transactionAmount, String transactionStatus) {
        this.transactionId = transactionId;
        this.transactionType = transactionType;
        this.transactionDateTime = transactionDateTime;
        this.fromAccountId = fromAccountId;
        this.transactionAmount = transactionAmount;
        this.transactionStatus = transactionStatus;
    }

    public TransactionResponseDTO(long transactionId, String transactionType, String transactionDateTime, long fromAccountId, long toAccountId, float transactionAmount, String transactionStatus) {
        this.transactionId = transactionId;
        this.transactionType = transactionType;
        this.transactionDateTime = transactionDateTime;
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.transactionAmount = transactionAmount;
        this.transactionStatus = transactionStatus;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(String transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public long getFromAccountId() {
        return fromAccountId;
    }

    public void setFromAccountId(long fromAccountId) {
        this.fromAccountId = fromAccountId;
    }

    public long getToAccountId() {
        return toAccountId;
    }

    public void setToAccountId(long toAccountId) {
        this.toAccountId = toAccountId;
    }

    public float getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(float transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }
}
