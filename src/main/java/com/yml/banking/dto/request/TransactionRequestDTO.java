package com.yml.banking.dto.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class TransactionRequestDTO {

    @NotNull
    @Pattern(regexp = "^(Withdraw|Deposit|Transfer)$")
    private String transactionType;

    private long fromAccountId;
    private long toAccountId;
    private float transactionAmount;

    public TransactionRequestDTO(String transactionType, long fromAccountId, long toAccountId, float transactionAmount) {
        this.transactionType = transactionType;
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public long getFromAccountId() {
        return fromAccountId;
    }

    public void setFromAccountId(long fromAccountId) {
        this.fromAccountId = fromAccountId;
    }

    public long getToAccountId() {
        return toAccountId;
    }

    public void setToAccountId(long toAccountId) {
        this.toAccountId = toAccountId;
    }

    public float getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(float transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

}
