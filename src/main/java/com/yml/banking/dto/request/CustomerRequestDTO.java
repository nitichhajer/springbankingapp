package com.yml.banking.dto.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CustomerRequestDTO {

    @NotNull
    @Size(min = 1, max = 45)
    private String customerName;


    @NotNull
    @Size(min = 1, max = 80)
    private String password;

    private int customerAge;

    @NotNull
    @Pattern(regexp = "^[0-9]{10}")
    private String customerPhone;

    @NotNull
    @Pattern(regexp = ".+[@].+[.].+")
    private String customerEmail;


    @NotNull
    @Size(min = 1, max = 60)
    private String customerAddress;

    private int customerZipCode;

    @NotNull
    @Pattern(regexp = "^[A-Z]{5}[0-9]{4}[A-Z]{1}")
    private String panNumber;

    public CustomerRequestDTO(String customerName, String password, int customerAge, String customerPhone, String customerEmail, String customerAddress, int customerZipCode, String panNumber) {
        this.customerName = customerName;
        this.password = password;
        this.customerAge = customerAge;
        this.customerPhone = customerPhone;
        this.customerEmail = customerEmail;
        this.customerAddress = customerAddress;
        this.customerZipCode = customerZipCode;
        this.panNumber = panNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCustomerAge() {
        return customerAge;
    }

    public void setCustomerAge(int customerAge) {
        this.customerAge = customerAge;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public int getCustomerZipCode() {
        return customerZipCode;
    }

    public void setCustomerZipCode(int customerZipCode) {
        this.customerZipCode = customerZipCode;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

}
