package com.yml.banking.dto.request;

import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class EmployeeRequestDTO {

    @NotNull
    @Size(min = 1, max = 45)
    private String employeeName;

    @NotNull
    @Size(min = 1, max = 80)
    private String password;

    private int employeeAge;

    @NotNull
    @Pattern(regexp = "^[0-9]{10}")
    private String employeePhone;

    @NotNull
    @Pattern(regexp = ".+[@].+[.].+")
    private String employeeEmail;

    @NotNull
    @Size(min = 1, max = 60)
    private String employeeAddress;

    private int employeeZipCode;

    @NotNull
    @Pattern(regexp = "^(Accountant|Auditor|Collector|Manager|Teller|Treasurer)$")
    private String designation;

    private long employeeSalary;
    private int branchId;

    public EmployeeRequestDTO(String employeeName, String password, int employeeAge, String employeePhone, String employeeEmail, String employeeAddress, int employeeZipCode, String designation, long employeeSalary, int branchId) {
        this.employeeName = employeeName;
        this.password = password;
        this.employeeAge = employeeAge;
        this.employeePhone = employeePhone;
        this.employeeEmail = employeeEmail;
        this.employeeAddress = employeeAddress;
        this.employeeZipCode = employeeZipCode;
        this.designation = designation;
        this.employeeSalary = employeeSalary;
        this.branchId = branchId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getEmployeeAge() {
        return employeeAge;
    }

    public void setEmployeeAge(int employeeAge) {
        this.employeeAge = employeeAge;
    }

    public String getEmployeePhone() {
        return employeePhone;
    }

    public void setEmployeePhone(String employeePhone) {
        this.employeePhone = employeePhone;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getEmployeeAddress() {
        return employeeAddress;
    }

    public void setEmployeeAddress(String employeeAddress) {
        this.employeeAddress = employeeAddress;
    }

    public int getEmployeeZipCode() {
        return employeeZipCode;
    }

    public void setEmployeeZipCode(int employeeZipCode) {
        this.employeeZipCode = employeeZipCode;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public long getEmployeeSalary() {
        return employeeSalary;
    }

    public void setEmployeeSalary(long employeeSalary) {
        this.employeeSalary = employeeSalary;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }
}
