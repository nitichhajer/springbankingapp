package com.yml.banking.dto.request;

import javax.validation.constraints.*;

public class BranchRequestDTO {

    @NotNull
    @Size(min = 1, max = 30)
    private String branchName;

    @NotNull
    @Size(min = 1, max = 60)
    private String branchAddress;

    private int branchZipCode;

    @NotNull
    @Pattern(regexp = "^[A-Z]{4}0[0-9]{6}")
    private String branchIfsc;

    public BranchRequestDTO(String branchName, String branchAddress, int branchZipCode, String branchIfsc) {
        this.branchName = branchName;
        this.branchAddress = branchAddress;
        this.branchZipCode = branchZipCode;
        this.branchIfsc = branchIfsc;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    public int getBranchZipCode() {
        return branchZipCode;
    }

    public void setBranchZipCode(int branchZipCode) {
        this.branchZipCode = branchZipCode;
    }

    public String getBranchIfsc() {
        return branchIfsc;
    }

    public void setBranchIfsc(String branchIfsc) {
        this.branchIfsc = branchIfsc;
    }
}
