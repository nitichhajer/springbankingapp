package com.yml.banking.dto.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class AccountRequestDTO {

    @NotNull
    @Pattern(regexp = "^(Savings|Current|Fixed)$")
    private String accountType;

    @NotNull
    @Pattern(regexp = "(\\d{4}-\\d{2}-\\d{2}|\\d{4}/\\d{2}/\\d{2})")
    private String openingDate;

    @NotNull
    @Pattern(regexp = "^(Active|Closed)$")
    private String accountStatus;

    @Pattern(regexp = "(\\d{4}-\\d{2}-\\d{2}|\\d{4}/\\d{2}/\\d{2})")
    private String closingDate;

    @NotNull
    private float accountBalance;

    @NotNull
    private int branchId;

    @NotNull
    private int customerId;

    public AccountRequestDTO(String accountType, String openingDate, String accountStatus, String closingDate, float accountBalance, int branchId, int customerId) {
        this.accountType = accountType;
        this.openingDate = openingDate;
        this.accountStatus = accountStatus;
        this.closingDate = closingDate;
        this.accountBalance = accountBalance;
        this.branchId = branchId;
        this.customerId = customerId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(String openingDate) {
        this.openingDate = openingDate;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(String closingDate) {
        this.closingDate = closingDate;
    }

    public float getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(float accountBalance) {
        this.accountBalance = accountBalance;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}
