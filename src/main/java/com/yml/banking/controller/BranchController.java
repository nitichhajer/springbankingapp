package com.yml.banking.controller;

import com.yml.banking.dto.request.BranchRequestDTO;
import com.yml.banking.dto.response.BranchResponseDTO;
import com.yml.banking.service.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/branches")
public class BranchController {

    private BranchService branchService;

    @Autowired
    public BranchController(BranchService branchService) {
        this.branchService = branchService;
    }

    @GetMapping
    public List<BranchResponseDTO> getBranches() {
        return branchService.getBranches();
    }

    @GetMapping("/{branch_id}")
    public BranchResponseDTO getBranch(@PathVariable("branch_id") int branchId) {
        return branchService.getBranch(branchId);
    }

    @PostMapping
    public BranchResponseDTO createBranch(@Valid @RequestBody BranchRequestDTO branchRequestDTO) {
        return branchService.createBranch(branchRequestDTO);
    }

    @PutMapping("/{branch_id}")
    public BranchResponseDTO updateBranch(@Valid @RequestBody BranchRequestDTO branchRequestDTO, @PathVariable("branch_id") int branchId) {
        return branchService.updateBranch(branchRequestDTO, branchId);
    }

    @DeleteMapping("/{branch_id}")
    public void deleteBranch(@PathVariable("branch_id") int branchId, HttpServletResponse httpServletResponse) {
        branchService.deleteBranch(branchId, httpServletResponse);
    }

}
