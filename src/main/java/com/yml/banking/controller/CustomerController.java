package com.yml.banking.controller;

import com.yml.banking.dto.request.CustomerRequestDTO;
import com.yml.banking.dto.response.CustomerResponseDTO;
import com.yml.banking.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    public List<CustomerResponseDTO> getCustomers() {
        return customerService.getCustomers();
    }

    @GetMapping("/{customer_id}")
    public CustomerResponseDTO getCustomer(@PathVariable("customer_id") int customerId) {
        return customerService.getCustomer(customerId);
    }

    @PostMapping
    public CustomerResponseDTO createCustomer(@Valid @RequestBody CustomerRequestDTO customerRequestDTO) {
        return customerService.createCustomer(customerRequestDTO);

    }

    @PutMapping("/{customer_id}")
    public CustomerResponseDTO updateCustomer(@Valid @RequestBody CustomerRequestDTO customerRequestDTO, @PathVariable("customer_id") int customerId) {
        return customerService.updateCustomer(customerRequestDTO, customerId);
    }

    @DeleteMapping("/{customer_id}")
    public void deleteCustomer(@PathVariable("customer_id") int customerId, HttpServletResponse httpServletResponse) {
        customerService.deleteCustomer(customerId, httpServletResponse);
    }

}

