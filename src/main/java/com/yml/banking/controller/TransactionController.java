package com.yml.banking.controller;

import com.yml.banking.dto.request.TransactionRequestDTO;
import com.yml.banking.dto.response.TransactionResponseDTO;
import com.yml.banking.service.transaction.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

    TransactionService transactionService;

    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping
    public List<TransactionResponseDTO> getTransactions() {
        return transactionService.getTransactions();
    }

    @GetMapping("/{transaction_id}")
    public TransactionResponseDTO getTransaction(@PathVariable("transaction_id") long transactionId) {
        return transactionService.getTransaction(transactionId);
    }

    @PostMapping
    public TransactionResponseDTO performTransaction(@Valid @RequestBody TransactionRequestDTO transactionRequestDTO)  {
        return transactionService.performTransaction(transactionRequestDTO);
    }

}
