package com.yml.banking.controller;

import com.yml.banking.dto.request.AccountRequestDTO;
import com.yml.banking.dto.response.AccountResponseDTO;
import com.yml.banking.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    private AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping
    public List<AccountResponseDTO> getAccounts() {
        return accountService.getAccounts();
    }

    @GetMapping("/{account_id}")
    public AccountResponseDTO getAccount(@PathVariable("account_id") long accountId) {
        return accountService.getAccount(accountId);
    }

    @PostMapping
    public AccountResponseDTO createAccount(@Valid @RequestBody AccountRequestDTO accountRequestDTO) {
        return accountService.createAccount(accountRequestDTO);
    }

    @PutMapping("/{account_id}")
    public AccountResponseDTO updateAccount(@Valid @RequestBody AccountRequestDTO accountRequestDTO, @PathVariable("account_id") long accountId) {
        return accountService.updateAccount(accountRequestDTO, accountId);
    }

    @DeleteMapping("/{account_id}")
    public void deleteAccount(@PathVariable("account_id") long accountId) {
        accountService.deleteAccount(accountId);
    }


}
