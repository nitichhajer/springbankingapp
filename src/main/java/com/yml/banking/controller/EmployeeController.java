package com.yml.banking.controller;

import com.yml.banking.dto.request.EmployeeRequestDTO;
import com.yml.banking.dto.response.EmployeeResponseDTO;
import com.yml.banking.entity.Employee;
import com.yml.banking.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private EmployeeService employeeService;

    @Autowired

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<EmployeeResponseDTO> getEmployees() {
        return employeeService.getEmployees();
    }

    @GetMapping("/{employee_id}")
    public EmployeeResponseDTO getEmployee(@PathVariable("employee_id") int employeeId) {
        return employeeService.getEmployee(employeeId);
    }

    @PostMapping
    public EmployeeResponseDTO createEmployee(@Valid @RequestBody EmployeeRequestDTO employeeRequestDTO) {
        return employeeService.createEmployee(employeeRequestDTO);
    }

    @PutMapping("/{employee_id}")
    public EmployeeResponseDTO updateEmployee(@Valid @RequestBody EmployeeRequestDTO employeeRequestDTO, @PathVariable("employee_id") int employeeId) {
        return employeeService.updateEmployee(employeeRequestDTO, employeeId);
    }

    @DeleteMapping("/{employee_id}")
    public void deleteEmployee(@PathVariable("employee_id") int employeeId, HttpServletResponse httpServletResponse) {
        employeeService.deleteEmployee(employeeId, httpServletResponse);
    }

    @GetMapping("filter/{branch_id}")
    public List<EmployeeResponseDTO> getEmployeesByBranch(@PathVariable("branch_id") int branchId) {
        return employeeService.getEmployeesByBranch(branchId);
    }


}
