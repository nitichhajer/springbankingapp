package com.yml.banking.repository;

import com.yml.banking.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    @Modifying
    @Query(value = "update account a set a.account_balance = ?2 where a.account_id = ?1 ", nativeQuery = true)
    void updateAccountBalanceByAccountId(long accountId, float balanceAmount);

}
