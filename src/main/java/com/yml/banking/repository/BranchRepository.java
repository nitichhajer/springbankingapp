package com.yml.banking.repository;

import com.yml.banking.entity.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BranchRepository extends JpaRepository <Branch, Integer> {

    Branch findBranchByBranchId(int branchId);
}
