package com.yml.banking.repository;

import com.yml.banking.entity.Branch;
import com.yml.banking.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    Optional<Object> findByEmployeeEmail(String email);

    List<Employee> findEmployeesByBranch(Branch branch);

}